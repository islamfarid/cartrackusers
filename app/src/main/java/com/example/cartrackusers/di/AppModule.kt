package com.example.cartrackusers.di

import com.example.cartrackusers.login.api.CountriesApi
import com.example.cartrackusers.users.api.UsersApi
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Module
import dagger.Provides
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class AppModule {
    companion object {
        @Provides
        @Reusable
        fun provideLoginCountriesApi(retrofit: Retrofit): CountriesApi =
            retrofit.create(CountriesApi::class.java)

        @Provides
        @Reusable
        fun provideUsersApi(retrofit: Retrofit): UsersApi =
            retrofit.create(UsersApi::class.java)

        @Provides
        @Reusable
        @DefaultCoroutineDispatcher
        fun provideDefaultCoroutineDispatcher(): CoroutineDispatcher = Dispatchers.Default

        @Provides
        @Reusable
        @IOCoroutineDispatcher
        fun provideIOCoroutineDispatcher(): CoroutineDispatcher = Dispatchers.IO

        private val json1 = Json {
            ignoreUnknownKeys = true
            prettyPrint = true
            explicitNulls = false
        }

        @ExperimentalSerializationApi
        @Provides
        @Singleton
        fun provideRetrofitClient(): Retrofit {
            val contentType = "application/json".toMediaType()
            val okHttpBuilder = OkHttpClient.Builder()
            val json = Json {
                ignoreUnknownKeys = true
                prettyPrint = true
                explicitNulls = false
            }.asConverterFactory(contentType)
            return Retrofit.Builder().baseUrl("https://restcountries.eu/rest/v1/all/")
                .addConverterFactory(json)
                .client(okHttpBuilder.build())
                .build()
        }
    }
}