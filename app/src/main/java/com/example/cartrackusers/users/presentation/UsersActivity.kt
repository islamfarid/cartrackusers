package com.example.cartrackusers.users.presentation

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cartrackusers.databinding.ActivityUsersBinding
import com.example.cartrackusers.users.model.ui.UsersUiState
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UsersActivity : AppCompatActivity() {
    private val usersViewModel: UsersViewModel by viewModels()
    private lateinit var binding: ActivityUsersBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUsersBinding.inflate(layoutInflater)
        setContentView(binding.root)
        listenToChanges()
        usersViewModel.onViewCreated()
    }

    private fun listenToChanges() {
        usersViewModel.uiState.observe(this) { uiState ->
            when (uiState) {
                is UsersUiState.UsersLoaded -> {
                    handleUsersLoaded(uiState)
                }
                is UsersUiState.Loading -> {
                    handleLoading()
                }
                is UsersUiState.Error -> {
                    applyErrorUi()
                }
            }
        }
    }

    private fun handleLoading() {
        binding.loading.isVisible = true
        binding.loading.isVisible = false
    }

    private fun applyErrorUi() {
        binding.loading.isVisible = false
        binding.loading.isVisible = true
    }

    private fun handleUsersLoaded(uiState: UsersUiState.UsersLoaded) {
        binding.loading.isVisible = false
        binding.loading.isVisible = false
        binding.usersRecyclerView.layoutManager = LinearLayoutManager(this)
        binding.usersRecyclerView.adapter = UserAdapter(uiState.users)
    }
}