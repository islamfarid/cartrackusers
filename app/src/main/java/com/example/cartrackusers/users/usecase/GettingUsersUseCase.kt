package com.example.cartrackusers.users.usecase

import com.example.cartrackusers.common.UseCase
import com.example.cartrackusers.di.IOCoroutineDispatcher
import com.example.cartrackusers.users.model.domain.User
import com.example.cartrackusers.users.repository.UsersRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GettingUsersUseCase @Inject constructor(
    private val usersRepository: UsersRepository,
    @IOCoroutineDispatcher private val CoroutineDispatcher: CoroutineDispatcher,
) : UseCase<Unit, List<User>> {
    override suspend fun execute(params: Unit?): List<User> = withContext(CoroutineDispatcher) {
        usersRepository.fetchUsers()
    }
}