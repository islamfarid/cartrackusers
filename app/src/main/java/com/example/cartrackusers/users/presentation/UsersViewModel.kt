package com.example.cartrackusers.users.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cartrackusers.common.Mapper
import com.example.cartrackusers.common.UseCase
import com.example.cartrackusers.di.DefaultCoroutineDispatcher
import com.example.cartrackusers.users.mappers.MapperUserUi
import com.example.cartrackusers.users.model.domain.User
import com.example.cartrackusers.users.model.ui.UserUiModel
import com.example.cartrackusers.users.model.ui.UsersUiState
import com.example.cartrackusers.users.usecase.UseCaseGettingUsers
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class UsersViewModel @Inject constructor(
    @UseCaseGettingUsers private val usersUseCase: UseCase<Unit, List<User>>,
    @MapperUserUi private val usersUiMapper: Mapper<User, UserUiModel>,
    @DefaultCoroutineDispatcher private val coroutineDispatcher: CoroutineDispatcher,
) : ViewModel() {
    private val _uiState = MutableLiveData<UsersUiState>()
    val uiState: LiveData<UsersUiState> = _uiState
    fun onViewCreated() {
        viewModelScope.launch {
            _uiState.value = UsersUiState.Loading
            try {
                notifyUsersLoaded(usersUseCase.execute(Unit))
            } catch (throwable: Throwable) {
                _uiState.value = UsersUiState.Error
            }
        }
    }

    private suspend fun notifyUsersLoaded(usersList: List<User>) {
        _uiState.value = withContext(coroutineDispatcher) {
            UsersUiState.UsersLoaded(usersList.map { usersUiMapper.map(it) })
        }
    }

}