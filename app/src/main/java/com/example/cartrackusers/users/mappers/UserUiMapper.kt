package com.example.cartrackusers.users.mappers

import com.example.cartrackusers.common.Mapper
import com.example.cartrackusers.users.model.domain.User
import com.example.cartrackusers.users.model.ui.UserUiModel
import javax.inject.Inject

class UserUiMapper @Inject constructor() : Mapper<User, UserUiModel> {
    override suspend fun map(from: User): UserUiModel {
        return UserUiModel(
            address = "Address : ${from.address}",
            company = "Company : ${from.company}",
            email = "Email : ${from.email}",
            name = "Name : ${from.name}",
            phone = "Phone : ${from.phone}",
            username = "UserName : ${from.username}",
            website = "Website : ${from.website}"
        )
    }
}