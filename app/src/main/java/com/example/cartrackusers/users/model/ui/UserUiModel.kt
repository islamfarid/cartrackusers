package com.example.cartrackusers.users.model.ui


data class UserUiModel(
    val address: String,
    val company: String,
    val email: String,
    val name: String,
    val phone: String,
    val username: String,
    val website: String
)