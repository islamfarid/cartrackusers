package com.example.cartrackusers.users.datasource

import com.example.cartrackusers.users.api.UsersApi
import com.example.cartrackusers.users.model.api.UserApiModel
import javax.inject.Inject

class UsersDataSourceImpl @Inject constructor(private val usersApi: UsersApi) : UsersDataSource {
    override suspend fun fetchUsers(): List<UserApiModel> =
        usersApi.fetchUsers(USERS_URL).body() ?: error(CAN_NOT_LOAD_USERS_LIST)

    companion object {
        const val CAN_NOT_LOAD_USERS_LIST =
            "CAN NOT LOAD USERS LIST" // Just as a simple as no error handler component
        const val USERS_URL = "https://jsonplaceholder.typicode.com/users/"
    }
}