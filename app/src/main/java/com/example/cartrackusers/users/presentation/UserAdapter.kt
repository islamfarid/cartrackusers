package com.example.cartrackusers.users.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.cartrackusers.databinding.ViewUserItemBinding
import com.example.cartrackusers.users.model.ui.UserUiModel

class UserAdapter(private val usersList: List<UserUiModel>) :
    RecyclerView.Adapter<UserAdapter.UsersViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersViewHolder {
        val binding = ViewUserItemBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return UsersViewHolder(binding)
    }

    override fun getItemCount() = usersList.size

    override fun onBindViewHolder(holder: UsersViewHolder, position: Int) {
        with(holder) {
            with(usersList[position]) {
                binding.nameTextView.text = name
                binding.emailTextView.text = email
                binding.companyTextView.text = company
                binding.websiteTextView.text = website
                binding.phoneTextView.text = phone
            }
        }
    }

    inner class UsersViewHolder(val binding: ViewUserItemBinding) :
        RecyclerView.ViewHolder(binding.root)

}