package com.example.cartrackusers.users.datasource

import com.example.cartrackusers.users.model.api.UserApiModel

interface UsersDataSource {
    suspend fun fetchUsers(): List<UserApiModel>

}