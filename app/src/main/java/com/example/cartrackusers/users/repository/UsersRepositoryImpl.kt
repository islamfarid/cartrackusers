package com.example.cartrackusers.users.repository

import com.example.cartrackusers.common.Mapper
import com.example.cartrackusers.users.datasource.UsersDataSource
import com.example.cartrackusers.users.mappers.MapperUserApi
import com.example.cartrackusers.users.model.api.UserApiModel
import com.example.cartrackusers.users.model.domain.User
import javax.inject.Inject

class UsersRepositoryImpl @Inject constructor(
    private val usersDataSource: UsersDataSource, @MapperUserApi private val userApiMapper:
    Mapper<UserApiModel, User>
) :
    UsersRepository {
    override suspend fun fetchUsers(): List<User> =
        usersDataSource.fetchUsers().map { userApiMapper.map(it) }
}