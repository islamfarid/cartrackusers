package com.example.cartrackusers.users.model.api


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class GeoApiModel(
    @SerialName("lat")
    val lat: String,
    @SerialName("lng")
    val lng: String
)