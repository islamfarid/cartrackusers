package com.example.cartrackusers.users.model.ui

sealed class UsersUiState {
    class UsersLoaded(val users: List<UserUiModel>) : UsersUiState()
    object Error : UsersUiState()
    object Loading : UsersUiState()
}