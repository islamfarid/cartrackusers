package com.example.cartrackusers.users.repository

import com.example.cartrackusers.users.model.domain.User

interface UsersRepository {
    suspend fun fetchUsers(): List<User>
}