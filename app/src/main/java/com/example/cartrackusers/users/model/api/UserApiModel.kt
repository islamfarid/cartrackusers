package com.example.cartrackusers.users.model.api


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class UserApiModel(
    @SerialName("address")
    val addressApiModel: AddressApiModel,
    @SerialName("company")
    val companyApiModel: CompanyApiModel,
    @SerialName("email")
    val email: String,
    @SerialName("id")
    val id: Int,
    @SerialName("name")
    val name: String,
    @SerialName("phone")
    val phone: String,
    @SerialName("username")
    val username: String,
    @SerialName("website")
    val website: String
)