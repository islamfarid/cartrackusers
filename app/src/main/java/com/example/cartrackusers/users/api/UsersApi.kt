package com.example.cartrackusers.users.api

import com.example.cartrackusers.users.model.api.UserApiModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface UsersApi {
    @GET
    suspend fun fetchUsers(@Url usersUrl: String): Response<List<UserApiModel>>
}