package com.example.cartrackusers.users.di

import com.example.cartrackusers.common.Mapper
import com.example.cartrackusers.common.UseCase
import com.example.cartrackusers.di.IOCoroutineDispatcher
import com.example.cartrackusers.users.datasource.UsersDataSource
import com.example.cartrackusers.users.datasource.UsersDataSourceImpl
import com.example.cartrackusers.users.mappers.MapperUserApi
import com.example.cartrackusers.users.mappers.MapperUserUi
import com.example.cartrackusers.users.mappers.UserApiMapper
import com.example.cartrackusers.users.mappers.UserUiMapper
import com.example.cartrackusers.users.model.api.UserApiModel
import com.example.cartrackusers.users.model.domain.User
import com.example.cartrackusers.users.model.ui.UserUiModel
import com.example.cartrackusers.users.repository.UsersRepository
import com.example.cartrackusers.users.repository.UsersRepositoryImpl
import com.example.cartrackusers.users.usecase.GettingUsersUseCase
import com.example.cartrackusers.users.usecase.UseCaseGettingUsers
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher

@Module
@InstallIn(SingletonComponent::class)
abstract class UsersModule {
    @Binds
    @Reusable
    abstract fun bindUsersDataSource(usersDataSourceImpl: UsersDataSourceImpl): UsersDataSource

    @Binds
    @Reusable
    abstract fun bindUsersRepository(usersRepositoryImpl: UsersRepositoryImpl): UsersRepository

    @Binds
    @MapperUserApi
    abstract fun bindUserApiMapper(userApiMapper: UserApiMapper): Mapper<UserApiModel, User>

    @Binds
    @MapperUserUi
    abstract fun bindCountriesUiMapper(userUiMapper: UserUiMapper): Mapper<User, UserUiModel>

    companion object {
        @Provides
        @UseCaseGettingUsers
        fun provideGettingUsersUseCase(
            usersRepository: UsersRepository,
            @IOCoroutineDispatcher coroutineDispatcher: CoroutineDispatcher
        ): UseCase<Unit, List<User>> {
            return GettingUsersUseCase(usersRepository, coroutineDispatcher)
        }

    }
}