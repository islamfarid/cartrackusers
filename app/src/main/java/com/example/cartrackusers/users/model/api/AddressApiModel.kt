package com.example.cartrackusers.users.model.api


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class AddressApiModel(
    @SerialName("city")
    val city: String,
    @SerialName("geo")
    val geoApiModel: GeoApiModel,
    @SerialName("street")
    val street: String,
    @SerialName("suite")
    val suite: String,
    @SerialName("zipcode")
    val zipcode: String
)