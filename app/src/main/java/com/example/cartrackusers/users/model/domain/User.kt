package com.example.cartrackusers.users.model.domain


data class User(
    val address: String,
    val company: String,
    val email: String,
    val id: Int,
    val name: String,
    val phone: String,
    val username: String,
    val website: String
)