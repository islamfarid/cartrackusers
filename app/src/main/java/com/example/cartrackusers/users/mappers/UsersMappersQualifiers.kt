package com.example.cartrackusers.users.mappers

import javax.inject.Qualifier

@Qualifier
annotation class MapperUserApi

@Qualifier
annotation class MapperUserUi