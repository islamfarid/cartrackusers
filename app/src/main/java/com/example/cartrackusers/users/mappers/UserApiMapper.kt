package com.example.cartrackusers.users.mappers

import com.example.cartrackusers.common.Mapper
import com.example.cartrackusers.users.model.api.UserApiModel
import com.example.cartrackusers.users.model.domain.User
import javax.inject.Inject

class UserApiMapper @Inject constructor() : Mapper<UserApiModel, User> {
    override suspend fun map(from: UserApiModel): User {
        return User(
            address = """${from.addressApiModel.city} / ${from.addressApiModel.street}""",
            company = from.companyApiModel.name,
            email = from.email,
            id = from.id,
            name = from.name,
            phone = from.phone,
            username = from.username,
            website = from.website
        )
    }
}