package com.example.cartrackusers.login.datasource

import com.example.cartrackusers.login.model.api.CountryApiModel

interface LoginDataSource {
    suspend fun fetchCountries(): List<CountryApiModel>
    suspend fun applyInsertedCredentials(isInserted: Boolean)
    suspend fun isCredentials(): Boolean
}