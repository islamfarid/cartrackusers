package com.example.cartrackusers.login.mappers

import com.example.cartrackusers.common.Mapper
import com.example.cartrackusers.login.model.domain.Country
import com.example.cartrackusers.login.model.ui.CountryUiModel
import javax.inject.Inject

class CountriesUiMapper @Inject constructor() : Mapper<Country, CountryUiModel> {
    override suspend fun map(from: Country): CountryUiModel {
        return CountryUiModel(from.name)
    }
}