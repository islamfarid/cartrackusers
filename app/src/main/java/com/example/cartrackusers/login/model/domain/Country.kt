package com.example.cartrackusers.login.model.domain

data class Country(val name: String, val capital: String?, val numericCode: String?)