package com.example.cartrackusers.login.mappers

import com.example.cartrackusers.common.Mapper
import com.example.cartrackusers.login.model.api.CountryApiModel
import com.example.cartrackusers.login.model.domain.Country
import javax.inject.Inject

class CountriesApiMapper @Inject constructor() : Mapper<CountryApiModel, Country> {
    override suspend fun map(from: CountryApiModel): Country {
        return Country(from.name, from.capital, from.numericCode)
    }
}