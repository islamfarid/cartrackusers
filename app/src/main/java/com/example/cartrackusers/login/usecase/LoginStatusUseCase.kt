package com.example.cartrackusers.login.usecase

import com.example.cartrackusers.common.UseCase
import com.example.cartrackusers.login.repository.LoginRepository
import javax.inject.Inject

class LoginStatusUseCase @Inject constructor(
    private val loginRepository: LoginRepository,
) : UseCase<Unit, Boolean> {
    override suspend fun execute(params: Unit?): Boolean =
        loginRepository.isCredentials() // Data store is safe to call even from UI thread
}