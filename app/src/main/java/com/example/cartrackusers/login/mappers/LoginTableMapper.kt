package com.example.cartrackusers.login.mappers

import com.example.cartrackusers.common.Mapper
import com.example.cartrackusers.login.model.db.LoginTableModel
import com.example.cartrackusers.login.model.domain.CurrentUser
import javax.inject.Inject

class LoginTableMapper @Inject constructor() : Mapper<LoginTableModel, CurrentUser> {
    override suspend fun map(from: LoginTableModel): CurrentUser {
        return CurrentUser(from.Username, from.Password)
    }
}