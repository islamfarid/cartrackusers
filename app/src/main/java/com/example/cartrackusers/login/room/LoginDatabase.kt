package com.example.cartrackusers.login.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.cartrackusers.login.model.db.LoginTableModel

@Database(entities = [LoginTableModel::class], version = 1, exportSchema = false)
abstract class LoginDatabase : RoomDatabase() {

    abstract fun loginDao(): DAOAccess

    companion object {

        @Volatile
        private lateinit var INSTANCE: LoginDatabase

        fun getDataseClient(context: Context): LoginDatabase {

            if (::INSTANCE.isInitialized) return INSTANCE

            synchronized(this) {

                INSTANCE = Room
                    .databaseBuilder(context, LoginDatabase::class.java, "LOGIN_DATABASE")
                    .fallbackToDestructiveMigration()
                    .build()

                return INSTANCE
            }
        }

    }

}