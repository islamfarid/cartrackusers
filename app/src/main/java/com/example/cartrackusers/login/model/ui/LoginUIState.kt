package com.example.cartrackusers.login.model.ui

sealed class LoginUIState {
    class CountriesLoaded(val countries: List<CountryUiModel>) : LoginUIState()
    object SetUp : LoginUIState()
    object Loading : LoginUIState()
    object Login : LoginUIState()
    object LoginSuccess : LoginUIState()
    object LoginFailure : LoginUIState()
}