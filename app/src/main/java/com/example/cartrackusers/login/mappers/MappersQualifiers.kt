package com.example.cartrackusers.login.mappers

import javax.inject.Qualifier

@Qualifier
annotation class MapperCountriesApi

@Qualifier
annotation class MapperCountriesUi

@Qualifier
annotation class MapperLoginTable