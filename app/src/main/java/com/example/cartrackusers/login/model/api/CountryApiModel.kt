package com.example.cartrackusers.login.model.api

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable()
data class CountryApiModel(
    @SerialName("name") val name: String,
    @SerialName("capital") val capital: String?,
    @SerialName("numericCode") val numericCode: String?
)