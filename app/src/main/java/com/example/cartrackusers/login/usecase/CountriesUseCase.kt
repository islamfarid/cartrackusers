package com.example.cartrackusers.login.usecase

import com.example.cartrackusers.common.UseCase
import com.example.cartrackusers.di.IOCoroutineDispatcher
import com.example.cartrackusers.login.model.domain.Country
import com.example.cartrackusers.login.repository.LoginRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class CountriesUseCase @Inject constructor(
    private val loginRepository: LoginRepository,
    @IOCoroutineDispatcher private val CoroutineDispatcher: CoroutineDispatcher,
) : UseCase<Unit, List<Country>> {
    override suspend fun execute(params: Unit?): List<Country> {
        return withContext(CoroutineDispatcher) {
            loginRepository.fetchCountries()
        }
    }
}