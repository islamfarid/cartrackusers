package com.example.cartrackusers.login.presentation

import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.example.cartrackusers.R
import com.example.cartrackusers.databinding.ActivityLoginBinding
import com.example.cartrackusers.di.IOCoroutineDispatcher
import com.example.cartrackusers.login.api.CountriesApi
import com.example.cartrackusers.login.model.ui.CountryUiModel
import com.example.cartrackusers.login.model.ui.LoginUIState
import com.example.cartrackusers.users.presentation.UsersActivity
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject


@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {

    private val loginViewModel: LoginViewModel by viewModels()
    private lateinit var binding: ActivityLoginBinding

    @Inject
    lateinit var countriesApi: CountriesApi

    @Inject
    @IOCoroutineDispatcher
    lateinit var coroutineDispatcher: CoroutineDispatcher
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        listenToChanges()
        loginViewModel.onViewCreated()
        handleClicks()
    }

    private fun handleClicks() {
        binding.btnAddLogin.setOnClickListener {
            val userName = binding.txtUsername.text.toString()
            val password = binding.txtPassword.text.toString()
            if (userName.isNotEmpty() && userName.isNotBlank() && password.isNotEmpty() && password.isNotBlank()) {
                loginViewModel.onLogin(userName, password)
            } else {
                Snackbar.make(
                    binding.root,
                    getString(R.string.empty_username_or_password),
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun listenToChanges() {
        loginViewModel.loginUiState.observe(this) { uiState ->
            when (uiState) {
                is LoginUIState.CountriesLoaded -> {
                    handleCountryListLoaded(uiState)
                }
                is LoginUIState.Login -> {
                    applyLoginNeededUI()
                }
                is LoginUIState.SetUp -> {
                    applyCredentialSetUpUI()
                }
                is LoginUIState.Loading -> {
                    binding.loading.isVisible = true
                }
                is LoginUIState.LoginFailure -> {
                    handleLoginFailed()
                }
                is LoginUIState.LoginSuccess -> {
                    handleLoginSuccess()
                }
            }
        }
    }

    private fun handleLoginSuccess() {
        binding.loading.isVisible = false
        startActivity(Intent(this, UsersActivity::class.java))
        finishAffinity()
    }

    private fun handleLoginFailed() {
        binding.loading.isVisible = false
        Snackbar.make(
            binding.root,
            getString(R.string.failed_login),
            Snackbar.LENGTH_SHORT
        ).show()
    }

    private fun applyCredentialSetUpUI() {
        binding.loading.isVisible = false
        binding.lblInsertHeading.text = getString(R.string.credentia_setup_needed)
        binding.btnAddLogin.text = getString(R.string.setup)
        emptyFields()
    }

    private fun applyLoginNeededUI() {
        binding.loading.isVisible = false
        binding.lblInsertHeading.text = getString(R.string.login_needed)
        binding.btnAddLogin.text = getString(R.string.login)
        emptyFields()
    }

    private fun emptyFields() {
        binding.txtUsername.setText("")
        binding.txtPassword.setText("")
    }

    private fun handleCountryListLoaded(loginUiState: LoginUIState.CountriesLoaded) {
        binding.loading.isVisible = false
        val arrayAdapter: ArrayAdapter<CountryUiModel> =
            ArrayAdapter<CountryUiModel>(
                this,
                android.R.layout.simple_spinner_item,
                loginUiState.countries
            )
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.countriesSpinner.adapter = arrayAdapter
    }
}