package com.example.cartrackusers.login.usecase

import javax.inject.Qualifier

@Qualifier
annotation class UseCaseCountries

@Qualifier
annotation class UseLoginStatus

@Qualifier
annotation class UseCaseLoginSetUp

@Qualifier
annotation class UseCaseLogin