package com.example.cartrackusers.login.repository

import com.example.cartrackusers.common.Mapper
import com.example.cartrackusers.login.datasource.LoginDataSource
import com.example.cartrackusers.login.mappers.MapperCountriesApi
import com.example.cartrackusers.login.mappers.MapperLoginTable
import com.example.cartrackusers.login.model.api.CountryApiModel
import com.example.cartrackusers.login.model.db.LoginTableModel
import com.example.cartrackusers.login.model.domain.Country
import com.example.cartrackusers.login.model.domain.CurrentUser
import com.example.cartrackusers.login.room.LoginDatabase
import javax.inject.Inject

class LoginRepositoryImpl @Inject constructor(
    private val dataSource: LoginDataSource,
    private val loginDatabase: LoginDatabase,
    @MapperCountriesApi private val mapper: Mapper<CountryApiModel, Country>,
    @MapperLoginTable private val loginTableMapper: Mapper<LoginTableModel, CurrentUser>,
) : LoginRepository {
    override suspend fun fetchCountries(): List<Country> {
        return dataSource.fetchCountries().map { mapper.map(it) }
    }

    override suspend fun insertData(username: String, password: String) {
        loginDatabase.loginDao().InsertData(LoginTableModel(username, password))
    }

    override suspend fun getLoginDetails(username: String): CurrentUser =
        loginTableMapper.map(loginDatabase.loginDao().getLoginDetails(username))


    override suspend fun applyInsertedCredentials(isInserted: Boolean) {
        dataSource.applyInsertedCredentials(isInserted)
    }

    override suspend fun isCredentials(): Boolean = dataSource.isCredentials()
}