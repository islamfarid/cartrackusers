package com.example.cartrackusers.login.model.domain

data class CurrentUser(val userName: String, val password: String)