package com.example.cartrackusers.login.datasource

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import com.example.cartrackusers.login.api.CountriesApi
import com.example.cartrackusers.login.model.api.CountryApiModel
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class LoginDataSourceImpl @Inject constructor(
    private val countriesApi: CountriesApi,
    private val loginDataStore: DataStore<Preferences>
) : LoginDataSource {
    override suspend fun fetchCountries(): List<CountryApiModel> {
        return countriesApi.fetchCountries(COUNTRIES_URL).body() ?: error(SOMETHING_WENT_WRONG)
    }

    override suspend fun applyInsertedCredentials(isInserted: Boolean) {
        loginDataStore.edit { settings ->
            settings[IS_INSERTED] = isInserted
        }
    }

    override suspend fun isCredentials() = loginDataStore.data
        .map { preferences ->
            preferences[IS_INSERTED] ?: false
        }.first()

    companion object {
        const val SOMETHING_WENT_WRONG =
            "SOMETHING WENT WRONG" // Just as a simple as no error handler component
        const val COUNTRIES_URL = "https://restcountries.com/v2/all"
        private const val IS_INSERTED_KEY = "is_inserted_key"
        val IS_INSERTED = booleanPreferencesKey(IS_INSERTED_KEY)
    }
}