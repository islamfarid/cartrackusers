package com.example.cartrackusers.login.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cartrackusers.common.Mapper
import com.example.cartrackusers.common.UseCase
import com.example.cartrackusers.di.DefaultCoroutineDispatcher
import com.example.cartrackusers.login.mappers.MapperCountriesUi
import com.example.cartrackusers.login.model.domain.Country
import com.example.cartrackusers.login.model.domain.CurrentUser
import com.example.cartrackusers.login.model.ui.CountryUiModel
import com.example.cartrackusers.login.model.ui.LoginUIState
import com.example.cartrackusers.login.usecase.UseCaseCountries
import com.example.cartrackusers.login.usecase.UseCaseLogin
import com.example.cartrackusers.login.usecase.UseCaseLoginSetUp
import com.example.cartrackusers.login.usecase.UseLoginStatus
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    @UseCaseCountries private val countriesUseCase: UseCase<Unit, List<Country>>,
    @UseLoginStatus private val loginStatusUseCase: UseCase<Unit, Boolean>,
    @UseCaseLogin private val loginUseCase: UseCase<CurrentUser, Boolean>,
    @UseCaseLoginSetUp private val loginSetUpUseCase: UseCase<CurrentUser, Boolean>,
    @MapperCountriesUi private val countriesMapper: Mapper<Country, CountryUiModel>,
    @DefaultCoroutineDispatcher private val coroutineDispatcher: CoroutineDispatcher,
) : ViewModel() {

    private val _uiState = MutableLiveData<LoginUIState>()
    val loginUiState: LiveData<LoginUIState> = _uiState
    private var isSetUpNeeded = true
    fun onViewCreated() {
        viewModelScope.launch {
            launch {
                _uiState.value = LoginUIState.Loading
                isSetUpNeeded = loginStatusUseCase.execute(Unit).not()
                notifyLoginStatus(isSetUpNeeded)
            }
            launch {
                notifyCountries(countriesUseCase.execute(Unit))
            }
        }
    }

    fun onLogin(userName: String, password: String) {
        viewModelScope.launch {
            _uiState.value = LoginUIState.Loading
            if (isSetUpNeeded) {
                handleSetUp(userName, password)
            } else {
                handleLogin(userName, password)
            }
        }
    }

    private suspend fun handleSetUp(userName: String, password: String) {
        isSetUpNeeded = loginSetUpUseCase.execute(CurrentUser(userName, password)).not()
        notifyLoginStatus(isSetUpNeeded)
    }

    private suspend fun handleLogin(userName: String, password: String) {
        val isLoggedIn = loginUseCase.execute(CurrentUser(userName, password))
        if (isLoggedIn) {
            _uiState.value = LoginUIState.LoginSuccess
        } else {
            _uiState.value = LoginUIState.LoginFailure
        }
    }

    private fun notifyLoginStatus(isSetUpNeeded: Boolean) {
        _uiState.value = if (!isSetUpNeeded) LoginUIState.Login else LoginUIState.SetUp
    }

    private fun notifyCountries(countries: List<Country>) {
        viewModelScope.launch {
            _uiState.value = withContext(coroutineDispatcher) {
                LoginUIState.CountriesLoaded(countries.map {
                    countriesMapper.map(it)
                })
            }
        }
    }
}