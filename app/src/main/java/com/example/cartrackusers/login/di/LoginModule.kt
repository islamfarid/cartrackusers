package com.example.cartrackusers.login.di

import android.app.Application
import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import com.example.cartrackusers.common.Mapper
import com.example.cartrackusers.common.UseCase
import com.example.cartrackusers.di.IOCoroutineDispatcher
import com.example.cartrackusers.login.datasource.LoginDataSource
import com.example.cartrackusers.login.datasource.LoginDataSourceImpl
import com.example.cartrackusers.login.mappers.*
import com.example.cartrackusers.login.model.api.CountryApiModel
import com.example.cartrackusers.login.model.db.LoginTableModel
import com.example.cartrackusers.login.model.domain.Country
import com.example.cartrackusers.login.model.domain.CurrentUser
import com.example.cartrackusers.login.model.ui.CountryUiModel
import com.example.cartrackusers.login.repository.LoginRepository
import com.example.cartrackusers.login.repository.LoginRepositoryImpl
import com.example.cartrackusers.login.room.LoginDatabase
import com.example.cartrackusers.login.usecase.*
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher

const val LOGIN_SETTINGS = "login_settings"
val Context.loginDataStore: DataStore<Preferences> by preferencesDataStore(name = LOGIN_SETTINGS)

@Module
@InstallIn(SingletonComponent::class)
abstract class LoginModule {
    @Binds
    @Reusable
    abstract fun bindLoginDataSource(loginDataSourceImpl: LoginDataSourceImpl): LoginDataSource

    @Binds
    @MapperCountriesApi
    abstract fun bindCountriesApiMapper(countriesApiMapper: CountriesApiMapper): Mapper<CountryApiModel, Country>

    @Binds
    @MapperCountriesUi
    abstract fun bindCountriesUiMapper(countriesUiMapper: CountriesUiMapper): Mapper<Country, CountryUiModel>

    @Binds
    @MapperLoginTable
    abstract fun bindLoginTableMapper(loginTableMapper: LoginTableMapper): Mapper<LoginTableModel, CurrentUser>

    companion object {

        @Provides
        @UseCaseCountries
        fun provideCountriesUseCase(
            loginRepository: LoginRepository,
            @IOCoroutineDispatcher coroutineDispatcher: CoroutineDispatcher
        ): UseCase<Unit, List<Country>> {
            return CountriesUseCase(loginRepository, coroutineDispatcher)
        }

        @Provides
        @UseCaseLoginSetUp
        fun provideLoginSetUpUseCase(
            loginRepository: LoginRepository,
            @IOCoroutineDispatcher coroutineDispatcher: CoroutineDispatcher
        ): UseCase<CurrentUser, Boolean> {
            return LoginSetUpUseCase(loginRepository, coroutineDispatcher)
        }

        @Provides
        @UseCaseLogin
        fun provideLoginUseCase(
            loginRepository: LoginRepository,
            @IOCoroutineDispatcher coroutineDispatcher: CoroutineDispatcher
        ): UseCase<CurrentUser, Boolean> {
            return LoginUseCase(loginRepository, coroutineDispatcher)
        }

        @Provides
        @UseLoginStatus
        fun provideCLoginStatusUseCase(
            loginRepository: LoginRepository,
        ): UseCase<Unit, Boolean> {
            return LoginStatusUseCase(loginRepository)
        }

        @Provides
        @Reusable
        fun provideLoginRepository(
            gameLocalDataSource: LoginDataSource,
            loginDatabase: LoginDatabase,
            @MapperCountriesApi mapper: Mapper<CountryApiModel, Country>,
            @MapperLoginTable loginTableMapper: Mapper<LoginTableModel, CurrentUser>,
        ): LoginRepository {
            return LoginRepositoryImpl(gameLocalDataSource, loginDatabase, mapper, loginTableMapper)
        }

        @Provides
        @Reusable
        fun provideLoginDb(application: Application): LoginDatabase {
            return LoginDatabase.getDataseClient(application.applicationContext)
        }

        @Provides
        @Reusable
        fun provideLoginDataStore(application: Application): DataStore<Preferences> {
            return application.applicationContext.loginDataStore
        }


    }
}