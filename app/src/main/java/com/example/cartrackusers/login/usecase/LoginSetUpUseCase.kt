package com.example.cartrackusers.login.usecase

import com.example.cartrackusers.common.UseCase
import com.example.cartrackusers.di.IOCoroutineDispatcher
import com.example.cartrackusers.login.model.domain.CurrentUser
import com.example.cartrackusers.login.repository.LoginRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class LoginSetUpUseCase @Inject constructor(
    private val loginRepository: LoginRepository,
    @IOCoroutineDispatcher private val CoroutineDispatcher: CoroutineDispatcher,
) : UseCase<CurrentUser, Boolean> {
    override suspend fun execute(params: CurrentUser?): Boolean {
        return params?.let { user ->
            return withContext(CoroutineDispatcher) {
                try {
                    loginRepository.insertData(user.userName, user.password)
                    loginRepository.applyInsertedCredentials(true)
                    true
                } catch (throwable: Throwable) {
                    false
                }
            }
        } ?: false
    }
}