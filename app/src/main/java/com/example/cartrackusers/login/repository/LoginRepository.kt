package com.example.cartrackusers.login.repository

import com.example.cartrackusers.login.model.domain.Country
import com.example.cartrackusers.login.model.domain.CurrentUser

interface LoginRepository {
    suspend fun fetchCountries(): List<Country>
    suspend fun insertData(username: String, password: String)
    suspend fun getLoginDetails(username: String): CurrentUser
    suspend fun applyInsertedCredentials(isInserted: Boolean)
    suspend fun isCredentials(): Boolean
}