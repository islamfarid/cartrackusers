package com.example.cartrackusers.login.api

import com.example.cartrackusers.login.model.api.CountryApiModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface CountriesApi {
    @GET
    suspend fun fetchCountries(@Url countriesUrl: String): Response<List<CountryApiModel>>
}