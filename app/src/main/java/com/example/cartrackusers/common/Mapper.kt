package com.example.cartrackusers.common

interface Mapper<Params, Result> {
    suspend fun map(from: Params): Result
}