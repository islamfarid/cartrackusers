package com.example.cartrackusers.common

interface UseCase<Params, Result> {
    suspend fun execute(params: Params?): Result
}