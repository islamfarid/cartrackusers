package com.example.cartrackusers.login.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.cartrackusers.MainCoroutineRule
import com.example.cartrackusers.TestObserver
import com.example.cartrackusers.common.Mapper
import com.example.cartrackusers.common.UseCase
import com.example.cartrackusers.login.model.domain.Country
import com.example.cartrackusers.login.model.domain.CurrentUser
import com.example.cartrackusers.login.model.ui.CountryUiModel
import com.example.cartrackusers.login.model.ui.LoginUIState
import com.example.cartrackusers.testObserver
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import junit.framework.TestCase.assertFalse
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class LoginViewModelTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()
    private lateinit var viewModel: LoginViewModel

    private val countriesUseCase: UseCase<Unit, List<Country>> = mockk()
    private val loginStatusUseCase: UseCase<Unit, Boolean> = mockk()
    private val loginUseCase: UseCase<CurrentUser, Boolean> = mockk()
    private val loginSetUpUseCase: UseCase<CurrentUser, Boolean> = mockk()
    private val countriesMapper: Mapper<Country, CountryUiModel> = mockk()
    private val loginUiState: LoginUIState?
        get() = viewModel.loginUiState.value

    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        viewModel = LoginViewModel(
            countriesUseCase = countriesUseCase,
            loginStatusUseCase = loginStatusUseCase,
            loginUseCase = loginUseCase,
            loginSetUpUseCase = loginSetUpUseCase,
            countriesMapper = countriesMapper,
            coroutineDispatcher = mainCoroutineRule.testDispatcher
        )
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `GIVEN viewModel creation WHEN Login Activity created THEN  check for login setup`() =
        mainCoroutineRule.runBlockingTest {
            coEvery { loginStatusUseCase.execute(any()) } returns false
            coEvery { countriesUseCase.execute(Unit) } returns emptyList()

            viewModel.onViewCreated()

            coVerify { loginStatusUseCase.execute(Unit) }
        }

    @ExperimentalCoroutinesApi
    @Test
    fun `GIVEN setup needed  WHEN Login Activity created THEN  notify Ui with SetUp action`() =
        mainCoroutineRule.runBlockingTest {
            val uiStateObserver = viewModel.loginUiState.testObserver()
            coEvery { loginStatusUseCase.execute(any()) } returns false
            coEvery { countriesUseCase.execute(Unit) } returns emptyList()

            viewModel.onViewCreated()

            coVerify { loginStatusUseCase.execute(Unit) }
            assertTrue(uiStateObserver.observedValues.contains(LoginUIState.SetUp))
            assertFalse(uiStateObserver.observedValues.contains(LoginUIState.Login))
        }

    @ExperimentalCoroutinesApi
    @Test
    fun `GIVEN viewModel creation WHEN Login Activity created THEN  notify Ui with both SetUp and Countries`() =
        mainCoroutineRule.runBlockingTest {
            val uiStateObserver = viewModel.loginUiState.testObserver()
            coEvery { loginStatusUseCase.execute(any()) } returns false
            coEvery { countriesUseCase.execute(Unit) } returns emptyList()

            viewModel.onViewCreated()

            coVerify { loginStatusUseCase.execute(Unit) }
            assertTrue(uiStateObserver.observedValues.contains(LoginUIState.SetUp))
            verifyUiStatesContainsCountries(uiStateObserver)
        }

    private fun verifyUiStatesContainsCountries(uiStateObserver: TestObserver<LoginUIState>) {
        var containsCountriesEvent = false
        uiStateObserver.observedValues.forEach {
            if (it is LoginUIState.CountriesLoaded) {
                containsCountriesEvent = true
            }
        }
        assertTrue(containsCountriesEvent)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `GIVEN setup is not needed  WHEN Login Activity created THEN  notify Ui with Login action`() =
        mainCoroutineRule.runBlockingTest {
            val uiStateObserver = viewModel.loginUiState.testObserver()
            coEvery { loginStatusUseCase.execute(any()) } returns true
            coEvery { countriesUseCase.execute(Unit) } returns emptyList()

            viewModel.onViewCreated()

            coVerify { loginStatusUseCase.execute(Unit) }
            assertFalse(uiStateObserver.observedValues.contains(LoginUIState.SetUp))
            assertTrue(uiStateObserver.observedValues.contains(LoginUIState.Login))
        }

    @ExperimentalCoroutinesApi
    @Test
    fun `GIVEN userName and password ready  WHEN login mode, and user clicks login THEN call login usecase`() =
        mainCoroutineRule.runBlockingTest {
            val userName = "userName"
            val password = "password"
            coEvery { loginStatusUseCase.execute(any()) } returns true
            coEvery { countriesUseCase.execute(Unit) } returns emptyList()
            coEvery { loginUseCase.execute(CurrentUser(userName, password)) } returns true
            viewModel.onViewCreated()

            viewModel.onLogin(userName, password)

            coVerify { loginUseCase.execute(CurrentUser(userName, password)) }
        }

    @ExperimentalCoroutinesApi
    @Test
    fun `GIVEN userName and password ready  WHEN setup mode, and user clicks setup THEN call setup usecase`() =
        mainCoroutineRule.runBlockingTest {
            val userName = "userName"
            val password = "password"
            coEvery { loginStatusUseCase.execute(any()) } returns false
            coEvery { countriesUseCase.execute(Unit) } returns emptyList()
            coEvery { loginSetUpUseCase.execute(CurrentUser(userName, password)) } returns true
            viewModel.onViewCreated()

            viewModel.onLogin(userName, password)

            coVerify { loginSetUpUseCase.execute(CurrentUser(userName, password)) }
        }
}